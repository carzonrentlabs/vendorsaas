﻿using System;

/// <summary>
/// Summary description for OL_Category
/// </summary>
public class OL_Model
{
    public int ModelID { get; set; }
    public string ModelName { get; set; }
    public int CategoryID { get; set; }
    public int CarCompID { get; set; }
    public int SeatingCapacity { get; set; }
    public int ProviderID { get; set; }
    public int UserID { get; set; }
    public DateTime CreateDate { get; set; }
    public int CreatedBy { get; set; }
    public DateTime ModifyDate { get; set; }
    public int ModifiedBy { get; set; }
    public string CategoryName { get; set; }
}


public class OL_ModelID
{
    public int ModelID { get; set; }
    public string ModelName { get; set; }
    public int ProviderID { get; set; }
}


public class OL_CarCompID
{
    public int CarCompID { get; set; }
    public int ProviderID { get; set; }
    public string CarCompName { get; set; }
}
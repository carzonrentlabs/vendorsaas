﻿using System;
using System.IO;
using context = System.Web.HttpContext;

/// <summary>
/// Summary description for HelperFunctions
/// </summary>
public class HelperFunctions
{
    public HelperFunctions()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void LogErrorToLogFile(Exception Error, string Parameters)
    {
        try
        {
            string path = context.Current.Server.MapPath("~/ErrorLogging/");
            // check if  exists
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = path + DateTime.Today.ToString("dd-MMM-yy") + ".log";
            // check if file exist
            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }
            // log the error now
            using (StreamWriter writer = File.AppendText(path))
            {
                string error = "@" + DateTime.Now.ToString() + ",URL : " + context.Current.Request.Url.ToString() + ",Error :" + Error.ToString() + ",Parameters" + Parameters;
                writer.WriteLine(error);
                writer.Flush();
                writer.Close();
            }
        }
        catch
        {
        }
    }

    public static void LogInfoToLogFile(string info, string ModuleName)
    {
        try
        {
            string path = context.Current.Server.MapPath("~/InfoLogging/");
            // check if directory exists
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = path + DateTime.Today.ToString("dd-MMM-yy") + ".log";
            // check if file exist
            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }
            // log the error now
            using (StreamWriter writer = File.AppendText(path))
            {
                string error = "@" + ModuleName + ":[" + DateTime.Now.ToString() + "] URL: " + context.Current.Request.Url.ToString() + " | " + info;
                writer.WriteLine(error);
                writer.Flush();
                writer.Close();
            }
        }
        catch
        {
            //throw;
        }
    }
}
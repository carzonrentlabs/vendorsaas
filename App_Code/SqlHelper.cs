﻿using System;
using System.Xml;
using System.Configuration;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
//using MySql.Data.MySqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace VendorSAAS
{
    public class SqlHelper
    {
        public string CONENCTION_STRING = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        public string InstaCONENCTION_STRING = ConfigurationManager.ConnectionStrings["InstaConnectionString"].ConnectionString;
        public static string Instacon = ConfigurationManager.ConnectionStrings["InstaConnectionString"].ConnectionString;

        public static object ExecuteScalar(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {

            // Create & open a SqlConnection, and dispose of it after we are done
            using (SqlConnection con = new SqlConnection(Instacon))
            {
                con.Open();

                // Call the overload that takes a connection in place of the connection string
                return ExecuteScalar(con, commandType, commandText, commandParameters);
            }
        }

        public static object ExecuteScalar(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {
            if (connection == null) throw new ArgumentNullException("connection");

            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();

            bool mustCloseConnection = false;
            PrepareCommand(cmd, connection, (SqlTransaction)null, commandType, commandText, commandParameters, out mustCloseConnection);

            // Execute the command & return the results
            object retval = cmd.ExecuteScalar();

            // Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear();

            if (mustCloseConnection)
                connection.Close();

            return retval;
        }

        public DataSet ExecuteDataSet(string storedProcedure, params SqlParameter[] commandParameters)
        {

            SqlConnection InstaCon = new SqlConnection(InstaCONENCTION_STRING);
            string inputParams = "";
            try
            {
                InstaCon.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = InstaCon;
                cmd.CommandText = storedProcedure;
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (SqlParameter param in commandParameters)
                {
                    if (param == null) continue;
                    cmd.Parameters.Add(param);
                    // inputParams = inputParams + param.Value!=null?param.Value.ToString():"null"+", ";
                }
                SqlDataAdapter sqlAdap = new SqlDataAdapter(cmd);
                DataSet sqldataSet = new DataSet();
                sqlAdap.Fill(sqldataSet);

                return sqldataSet;

            }
            catch(Exception e)
            {
                //HelperFunctions.LogErrorToLogFile(e, "Error while fetching dataset: "+ storedProcedure+" | "+ inputParams);
                InstaCon.Close();
                return null;
            }
        }

        public float GetTotalEarningsForDay(int CarId, bool isVendorCar, DateTime evalDate)
        {
            SqlConnection InstaCon = new SqlConnection(InstaCONENCTION_STRING);
            try
            {
                SqlCommand command = InstaCon.CreateCommand();
                command.CommandText = "select isnull(sum(detail.TotalCost), 0) as TotalEarnings from CorIntCorMeterDetails detail, CorIntDutyAllocation alloc where detail.BookingID = alloc.BookingID and alloc.CarID = @CarId and alloc.VendorCarYN = @VendorCar and detail.PickUpDate = @CalculationDate";
                command.Parameters.AddWithValue("CarId", CarId);
                command.Parameters.AddWithValue("VendorCar", isVendorCar);
                command.Parameters.AddWithValue("CalculationDate", evalDate);
                InstaCon.Open();
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader != null)
                {
                    DataTable resultTable = new DataTable();
                    resultTable.Load(dataReader);
                    if (resultTable.Rows.Count > 0)
                    {
                        DataRow row = resultTable.Rows[0];
                        return float.Parse(row["TotalEarnings"].ToString(),
                            System.Globalization.CultureInfo.InvariantCulture);
                    }
                }
                return 0.0f;
            }
            catch (Exception Ex)
            {
                return 0.0f;
            }
            finally
            {
                if (InstaCon != null) InstaCon.Close();
            }
        }

        //add by bksharma on date 07 oct 2014
        public int ExecuteNonQuery(string storedProcedure, params SqlParameter[] commandParameters)
        {
            using (SqlConnection con = new SqlConnection(InstaCONENCTION_STRING))
            {
                try
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = storedProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter param in commandParameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                    return cmd.ExecuteNonQuery();
                       
                }
                catch { return -1; }
                finally { con.Close(); }
            }
        }

        public static int ExecuteNonQuery(CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {
            using (SqlConnection con = new SqlConnection(Instacon))
            {
                con.Open();

                // Call the overload that takes a connection in place of the connection string
                return ExecuteNonQuery(con, commandType, commandText, commandParameters);
            }
        }

        public static int ExecuteNonQuery(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {
            if (connection == null) throw new ArgumentNullException("connection");

            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            PrepareCommand(cmd, connection, (SqlTransaction)null, commandType, commandText, commandParameters, out mustCloseConnection);

            // Finally, execute the command
            int retval = 0;
            try
            {
                retval = cmd.ExecuteNonQuery();
            }

            // Detach the SqlParameters from the command object, so they can be used again
            catch (Exception ex)
            { }
            finally
            {
                cmd.Parameters.Clear();
                if (mustCloseConnection)
                    connection.Close();

            }
            return retval;
        }

        private static void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, SqlParameter[] commandParameters, out bool mustCloseConnection)
        {
            if (command == null) throw new ArgumentNullException("command");
            if (commandText == null || commandText.Length == 0) throw new ArgumentNullException("commandText");

            // If the provided connection is not open, we will open it
            if (connection.State != ConnectionState.Open)
            {
                mustCloseConnection = true;
                connection.Open();
            }
            else
            {
                mustCloseConnection = false;
            }

            //Sumit

            command.CommandTimeout = 0;

            // Associate the connection with the command
            command.Connection = connection;

            // Set the command text (stored procedure name or SQL statement)
            command.CommandText = commandText;

            // If we were provided a transaction, assign it
            if (transaction != null)
            {
                if (transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
                command.Transaction = transaction;
            }

            // Set the command type
            command.CommandType = commandType;

            // Attach the command parameters if they are provided
            if (commandParameters != null)
            {
                AttachParameters(command, commandParameters);
            }
            return;
        }

        private static void AttachParameters(SqlCommand command, SqlParameter[] commandParameters)
        {
            if (command == null) throw new ArgumentNullException("command");
            if (commandParameters != null)
            {
                foreach (SqlParameter p in commandParameters)
                {
                    if (p != null)
                    {
                        // Check for derived output value with no value assigned
                        if ((p.Direction == ParameterDirection.InputOutput ||
                            p.Direction == ParameterDirection.Input) &&
                            (p.Value == null))
                        {
                            p.Value = DBNull.Value;
                        }
                        command.Parameters.Add(p);
                    }
                }
            }
        }

        public static double convertDateTimeToEPOCH(DateTime dateTime)
        {
            return dateTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }
    }
}
﻿using System;
/// <summary>
/// Summary description for OL_Package
/// </summary>
public class OL_Package
{
    public int PkgID { get; set; }
    public int PkgHrs { get; set; }
    public int PkgKms { get; set; }
    public int ExtraHrRate { get; set; }
    public int ExtraKMRate { get; set; }
    public int NoOfNgts { get; set; }
    public bool IsMinuteWiseBilling { get; set; }
}

public class OL_PackageResponse
{
    public int PkgId { get; set; }
    public double PkgRate { get; set; }
    public double ExtraHrRate { get; set; }
    public double ExtraKMRate { get; set; }
    public double OutStationAllowance { get; set; }
    public double NightStayAllowance { get; set; }
    public double ThresholdExtraHr { get; set; }
    public double ThresholdExtraKM { get; set; }
    public double WaitingCharges { get; set; }
    public double ExNgtAmt { get; set; }
    public double ExdayAmt { get; set; }
    public double FxdTaxes { get; set; }
    public int PkgHrs { get; set; }
    public int PkgKms { get; set; }
    public int NoOfNgts { get; set; }
    public int NoOfDys { get; set; }
}

public class OL_PackageRequest
{
    public int ClientCoId { get; set; }
    public int CarCatId { get; set; }
    public double PkgHrs { get; set; }
    public int CityID { get; set; }
    public DateTime PickupDate { get; set; }
    public int PkgID { get; set; }
    public DateTime DropoffDate { get; set; }
    //public string Service { get; set; }
    //public bool OutstationYN { get; set; }
    //public bool CustomPkgYN { get; set; }
    public int LookupID { get; set; }
    public int NoOfDays { get; set; }
    public int NoOfNights { get; set; }
}

public class OL_PackageInterval
{
    public double PkgHrs { get; set; }
    public int NoOfDays { get; set; }
    public int NoOfNights { get; set; }
}

public class OL_IndicatedPrice
{
    public double IndicatedPrice;
    public double IndicatedNightStayAllowance;
    public double IndicatedOutstationAmount;
    public double CGSTPercent;
    public double SGSTPercent;
    public double IGSTPercent;
    public double CGSTaxAmount;
    public double SGSTaxAmount;
    public double IGSTaxAmount;
    public int ClientGSTId;
    public double IndicatedGSTSurchargeAmount;
}

public class OL_TaxHeads
{
    public double ServiceTaxPercent { get; set; }
    public double EduCessPercent { get; set; }
    public double HduCessPercent { get; set; }
    public double DSTPercent { get; set; }
    public bool KMWisePackage { get; set; }
    public double SwachhBharatTaxPercent { get; set; }
    public double KrishiKalyanTaxPercent { get; set; }
    public string GSTEnabledYN { get; set; }
    public double CGSTPercent { get; set; }
    public double SGSTPercent { get; set; }
    public double IGSTPercent { get; set; }
    public string ClientGSTId { get; set; }

}
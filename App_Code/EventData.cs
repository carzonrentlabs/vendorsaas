﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using VendorSAAS;

/// <summary>
/// Summary description for EventData
/// </summary>
public class EventData
{
    public EventData()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public MapWebMethods.GenericResponse SaveCity(OL_City city)
    {
        MapWebMethods.GenericResponse response = new MapWebMethods.GenericResponse();

        DataSet ds = new DataSet();
        try
        {
            SqlHelper sql = new SqlHelper();
            int index = -1;
            SqlParameter[] sqlParams = new SqlParameter[19];
            sqlParams[index += 1] = new SqlParameter("@CityName", city.CityName);
            sqlParams[index += 1] = new SqlParameter("@StateID", city.StateID);
            sqlParams[index += 1] = new SqlParameter("@CityLat", city.CityLat);
            sqlParams[index += 1] = new SqlParameter("@CityLon", city.CityLon);
            sqlParams[index += 1] = new SqlParameter("@ProviderID", city.ProviderID);
            sqlParams[index += 1] = new SqlParameter("@CorDriveYN", city.CorDriveYN);
            sqlParams[index += 1] = new SqlParameter("@Region", city.Region);
            sqlParams[index += 1] = new SqlParameter("@FName", city.FName);
            sqlParams[index += 1] = new SqlParameter("@MName", city.MName);
            sqlParams[index += 1] = new SqlParameter("@LName", city.LName);
            sqlParams[index += 1] = new SqlParameter("@DesigID", city.DesigID);
            sqlParams[index += 1] = new SqlParameter("@Address", city.Address);
            sqlParams[index += 1] = new SqlParameter("@Phone", city.Phone);
            sqlParams[index += 1] = new SqlParameter("@EmailID", city.EmailID);
            sqlParams[index += 1] = new SqlParameter("@TanNo", city.TanNo);
            sqlParams[index += 1] = new SqlParameter("@GSTIN", city.GSTIN);
            sqlParams[index += 1] = new SqlParameter("@GSTINAddress", city.GSTINAddress);
            sqlParams[index += 1] = new SqlParameter("@UserID", city.UserID);

            sqlParams[index += 1] = new SqlParameter("@CityID", SqlDbType.Int); //need to change it to output
            sqlParams[index].Direction = ParameterDirection.Output;

            ds = sql.ExecuteDataSet("SaveCity", sqlParams); //SaveCity Proc Here

            if (Convert.ToInt32(sqlParams[index].Value) == -1)
            {
                response.status = "Failure";
                response.failureMessage = "CityName already exists.";
            }
            if (Convert.ToInt32(sqlParams[index].Value) == 0)
            {
                response.status = "Failure";
                response.failureMessage = "Unable to Create City.";
            }
        }
        catch (Exception ex)
        {
            ds = null;
        }

        return response;
    }

    public MapWebMethods.GenericResponse UpdateCity(OL_City city)
    {
        MapWebMethods.GenericResponse response = new MapWebMethods.GenericResponse();

        DataSet ds = new DataSet();
        try
        {
            SqlHelper sql = new SqlHelper();
            int index = -1;
            SqlParameter[] sqlParams = new SqlParameter[20];
            sqlParams[index += 1] = new SqlParameter("@CityID", city.CityId);
            sqlParams[index += 1] = new SqlParameter("@CityName", city.CityName);
            sqlParams[index += 1] = new SqlParameter("@StateID", city.StateID);
            sqlParams[index += 1] = new SqlParameter("@CityLat", city.CityLat);
            sqlParams[index += 1] = new SqlParameter("@CityLon", city.CityLon);
            sqlParams[index += 1] = new SqlParameter("@ProviderID", city.ProviderID);
            sqlParams[index += 1] = new SqlParameter("@CorDriveYN", city.CorDriveYN);
            sqlParams[index += 1] = new SqlParameter("@Region", city.Region);
            sqlParams[index += 1] = new SqlParameter("@FName", city.FName);
            sqlParams[index += 1] = new SqlParameter("@MName", city.MName);
            sqlParams[index += 1] = new SqlParameter("@LName", city.LName);
            sqlParams[index += 1] = new SqlParameter("@DesigID", city.DesigID);
            sqlParams[index += 1] = new SqlParameter("@Address", city.Address);
            sqlParams[index += 1] = new SqlParameter("@Phone", city.Phone);
            sqlParams[index += 1] = new SqlParameter("@EmailID", city.EmailID);
            sqlParams[index += 1] = new SqlParameter("@TanNo", city.TanNo);
            sqlParams[index += 1] = new SqlParameter("@GSTIN", city.GSTIN);
            sqlParams[index += 1] = new SqlParameter("@GSTINAddress", city.GSTINAddress);
            sqlParams[index += 1] = new SqlParameter("@UserID", city.UserID);

            sqlParams[index += 1] = new SqlParameter("@ReturnStatus", SqlDbType.Int); //need to change it to output
            sqlParams[index].Direction = ParameterDirection.Output;

            if (Convert.ToInt32(sqlParams[index].Value) == -1)
            {
                response.status = "Failure";
                response.failureMessage = "CityName already exists.";
            }
            if (Convert.ToInt32(sqlParams[index].Value) == 0)
            {
                response.status = "Failure";
                response.failureMessage = "Unable to Update City.";
            }

            int status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UpdateCity", sqlParams);
        }
        catch (Exception ex)
        {
            ds = null;
        }

        return response;
    }

    public MapWebMethods.GenericResponse SaveCategory(OL_Category category)
    {
        MapWebMethods.GenericResponse response = new MapWebMethods.GenericResponse();
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"INSERT INTO CORIntCarCatMaster(CarCatName, CarCatGroup, CorporateDCOPercent,SelfDriveDCOPercent,HotelDCOPercent
            ,CCDCOPercent,Active,CreateDate, CreatedBy,ProviderID)
            VALUES(@CategoryName, @CategoryGroup, 0, 0, 0, 0, 1, getdate(), @UserID, @ProviderID)";

            command.Parameters.AddWithValue("CategoryName", category.CategoryName);
            command.Parameters.AddWithValue("CategoryGroup", category.CategoryGroup);
            command.Parameters.AddWithValue("ProviderID", category.ProviderID);
            command.Parameters.AddWithValue("UserID", category.UserID);

            InstaCon.Open();
            if (command.ExecuteNonQuery() > 0)
            {
                response.status = "Success";
            }
            else
            {
                response.status = "Failure";
                response.failureMessage = "Unable to Save data.";
            }
        }
        catch (Exception ex)
        {
            response.status = "Failure";
            response.failureMessage = ex.ToString();
        }

        return response;
    }

    public MapWebMethods.GenericResponse UpdateCategory(OL_Category category)
    {
        MapWebMethods.GenericResponse response = new MapWebMethods.GenericResponse();
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"update CORIntCarCatMaster set CarCatName = @CategoryName, CarCatGroup = @CategoryGroup
            , ModifyDate = Getdate(), ModifiedBy = @UserID, ProviderID = @ProviderID where CarCatID = @CategoryID ";

            command.Parameters.AddWithValue("CategoryName", category.CategoryName);
            command.Parameters.AddWithValue("CategoryGroup", category.CategoryGroup);
            command.Parameters.AddWithValue("ProviderID", category.ProviderID);
            command.Parameters.AddWithValue("UserID", category.UserID);
            command.Parameters.AddWithValue("CategoryID", category.CategoryID);

            InstaCon.Open();
            if (command.ExecuteNonQuery() > 0)
            {
                response.status = "Success";
            }
            else
            {
                response.status = "Failure";
                response.failureMessage = "Unable to Save data.";
            }
        }
        catch (Exception ex)
        {
            response.status = "Failure";
            response.failureMessage = ex.ToString();
        }

        return response;
    }

    public List<OL_Category> GetCategoryDetails(OL_Category category)
    {
        List<OL_Category> response = new List<OL_Category>();
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"select CarCatName, CarCatGroup, ProviderId, CreateDate,CreatedBy,ModifyDate,ModifiedBy from CORIntCarCatMaster where ProviderID = @ProviderID ";
            command.Parameters.AddWithValue("ProviderID", category.ProviderID);

            InstaCon.Open();
            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader != null)
            {
                DataTable resultTable = new DataTable();
                resultTable.Load(dataReader);
                if (resultTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < resultTable.Rows.Count; rowIndex++)
                    {
                        OL_Category rep = new OL_Category();
                        rep.CategoryName = Convert.ToString(resultTable.Rows[rowIndex]["CarCatName"]);
                        rep.CategoryGroup = Convert.ToString(resultTable.Rows[rowIndex]["CarCatGroup"]);
                        rep.ProviderID = Convert.ToInt32(resultTable.Rows[rowIndex]["ProviderId"]);

                        rep.CreateDate = Convert.ToDateTime(resultTable.Rows[rowIndex]["CreateDate"]);
                        rep.CreatedBy = Convert.ToInt32(resultTable.Rows[rowIndex]["CreatedBy"]);
                        rep.ModifyDate = Convert.ToDateTime(resultTable.Rows[rowIndex]["ModifyDate"]);
                        rep.ModifiedBy = Convert.ToInt32(resultTable.Rows[rowIndex]["ModifiedBy"]);

                        response.Add(rep);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

        return response;
    }


    public MapWebMethods.GenericResponse SaveModel(OL_Model model)
    {
        MapWebMethods.GenericResponse response = new MapWebMethods.GenericResponse();
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"INSERT INTO CORIntCarmodelMaster(CarModelName, CarCompID, CarCatID,SeatingCapacity,Active,CreateDate, CreatedBy,ProviderID)
            VALUES(@ModelName, @CarCompID, @CategoryID, @SeatingCapacity, 1, getdate(), @UserID, @ProviderID)";

            command.Parameters.AddWithValue("ModelName", model.ModelName);
            command.Parameters.AddWithValue("CarCompID", model.CarCompID);
            command.Parameters.AddWithValue("CategoryID", model.CategoryID);
            command.Parameters.AddWithValue("SeatingCapacity", model.SeatingCapacity);
            command.Parameters.AddWithValue("ProviderID", model.ProviderID);
            command.Parameters.AddWithValue("UserID", model.UserID);

            InstaCon.Open();
            if (command.ExecuteNonQuery() > 0)
            {
                response.status = "Success";
            }
            else
            {
                response.status = "Failure";
                response.failureMessage = "Unable to Save data.";
            }
        }
        catch (Exception ex)
        {
            response.status = "Failure";
            response.failureMessage = ex.ToString();
        }

        return response;
    }

    public MapWebMethods.GenericResponse UpdateModel(OL_Model model)
    {
        MapWebMethods.GenericResponse response = new MapWebMethods.GenericResponse();
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"update CORIntCarmodelMaster set CarModelName = @ModelName, CarCompID = @CarCompID
            , CarCatID = @CategoryID, SeatingCapacity = @SeatingCapacity, ProviderID = @ProviderID
            , ModifyDate = Getdate(), ModifiedBy = @UserID
            where CarModelID = @ModelID ";

            command.Parameters.AddWithValue("ModelName", model.ModelName);
            command.Parameters.AddWithValue("CarCompID", model.CarCompID);
            command.Parameters.AddWithValue("CategoryID", model.CategoryID);
            command.Parameters.AddWithValue("SeatingCapacity", model.SeatingCapacity);
            command.Parameters.AddWithValue("ProviderID", model.ProviderID);
            command.Parameters.AddWithValue("UserID", model.UserID);
            command.Parameters.AddWithValue("ModelID", model.ModelID);

            InstaCon.Open();
            if (command.ExecuteNonQuery() > 0)
            {
                response.status = "Success";
            }
            else
            {
                response.status = "Failure";
                response.failureMessage = "Unable to Save data.";
            }
        }
        catch (Exception ex)
        {
            response.status = "Failure";
            response.failureMessage = ex.ToString();
        }

        return response;
    }

    public List<OL_Model> GetModelDetails(OL_Model model)
    {
        List<OL_Model> response = new List<OL_Model>();
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"select CMM.CarModelName, CCM.CarCatName, CCM.ProviderId, CMM.CreateDate, CMM.CreatedBy, CMM.ModifyDate, CMM.ModifiedBy 
            from CorIntCarModelMaster as CMM 
            inner join CORIntCarCatMaster as CCM on CMM.CarCatID = CCM.CarCatID
            where CCM.ProviderID = @ProviderID ";
            command.Parameters.AddWithValue("ProviderID", model.ProviderID);

            InstaCon.Open();
            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader != null)
            {
                DataTable resultTable = new DataTable();
                resultTable.Load(dataReader);
                if (resultTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < resultTable.Rows.Count; rowIndex++)
                    {
                        OL_Model rep = new OL_Model();
                        rep.ModelName = Convert.ToString(resultTable.Rows[rowIndex]["CarModelName"]);
                        rep.CategoryName = Convert.ToString(resultTable.Rows[rowIndex]["CarCatName"]);
                        rep.ProviderID = Convert.ToInt32(resultTable.Rows[rowIndex]["ProviderId"]);

                        rep.CreateDate = Convert.ToDateTime(resultTable.Rows[rowIndex]["CreateDate"]);
                        rep.CreatedBy = Convert.ToInt32(resultTable.Rows[rowIndex]["CreatedBy"]);
                        rep.ModifyDate = Convert.ToDateTime(resultTable.Rows[rowIndex]["ModifyDate"]);
                        rep.ModifiedBy = Convert.ToInt32(resultTable.Rows[rowIndex]["ModifiedBy"]);

                        response.Add(rep);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

        return response;
    }

    public List<OL_CityID> GetCityList(OL_CityID city)
    {
        List<OL_CityID> response = new List<OL_CityID>();
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"select CityID, CityName from corintcitymaster where ProviderID = @ProviderID ";
            command.Parameters.AddWithValue("ProviderID", city.ProviderID);

            InstaCon.Open();
            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader != null)
            {
                DataTable resultTable = new DataTable();
                resultTable.Load(dataReader);
                if (resultTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < resultTable.Rows.Count; rowIndex++)
                    {
                        OL_CityID rep = new OL_CityID();
                        rep.CityId = Convert.ToInt32(resultTable.Rows[rowIndex]["CityID"]);
                        rep.CityName = Convert.ToString(resultTable.Rows[rowIndex]["CityName"]);
                        response.Add(rep);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

        return response;
    }


    public List<OL_StateID> GetStateList()
    {
        List<OL_StateID> response = new List<OL_StateID>();
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"select StateID, StateName from corintStatemaster where Active = 1 ";

            InstaCon.Open();
            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader != null)
            {
                DataTable resultTable = new DataTable();
                resultTable.Load(dataReader);
                if (resultTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < resultTable.Rows.Count; rowIndex++)
                    {
                        OL_StateID rep = new OL_StateID();
                        rep.StateID = Convert.ToInt32(resultTable.Rows[rowIndex]["StateID"]);
                        rep.StateName = Convert.ToString(resultTable.Rows[rowIndex]["StateName"]);
                        response.Add(rep);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

        return response;
    }

    public List<OL_CarCompID> GetCarCompList(OL_CarCompID carcomp)
    {
        List<OL_CarCompID> response = new List<OL_CarCompID>();
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"select CarCompID,CarCompName from CORIntCarCompMaster where active=1 ";
            //command.Parameters.AddWithValue("ProviderID", city.ProviderID);

            InstaCon.Open();
            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader != null)
            {
                DataTable resultTable = new DataTable();
                resultTable.Load(dataReader);
                if (resultTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < resultTable.Rows.Count; rowIndex++)
                    {
                        OL_CarCompID rep = new OL_CarCompID();
                        rep.CarCompID = Convert.ToInt32(resultTable.Rows[rowIndex]["CarCompID"]);
                        rep.CarCompName = Convert.ToString(resultTable.Rows[rowIndex]["CarCompName"]);
                        response.Add(rep);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

        return response;
    }
    public List<OL_ModelID> GetModelList(OL_ModelID model) //changes to be made based on the clientcoid
    {
        List<OL_ModelID> response = new List<OL_ModelID>();
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"select CMM.CarModelID, CMM.CarModelName from corintcarmodelmaster as CMM 
            inner join corintcarcatmaster as CCM on CMM.carcatid = CCM.carcatid where CCM.ProviderID = @ProviderID ";
            command.Parameters.AddWithValue("ProviderID", model.ProviderID);

            InstaCon.Open();
            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader != null)
            {
                DataTable resultTable = new DataTable();
                resultTable.Load(dataReader);
                if (resultTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < resultTable.Rows.Count; rowIndex++)
                    {
                        OL_ModelID rep = new OL_ModelID();
                        rep.ModelID = Convert.ToInt32(resultTable.Rows[rowIndex]["CityID"]);
                        rep.ModelName = Convert.ToString(resultTable.Rows[rowIndex]["CityName"]);
                        response.Add(rep);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

        return response;
    }

    public List<OL_ModelClientReponse> GetModel_Client(OL_ModelClientRequest request)
    {
        List<OL_ModelClientReponse> response = new List<OL_ModelClientReponse>();

        DataSet ds = new DataSet();
        try
        {
            SqlHelper sql = new SqlHelper();
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@flag", 2);
            sqlParams[1] = new SqlParameter("@CityID", request.CityID);
            sqlParams[2] = new SqlParameter("@ClientID", request.ClientCoID);
            ds = sql.ExecuteDataSet("ProcBook_SelModelDetails_1", sqlParams);

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    OL_ModelClientReponse rsp = new OL_ModelClientReponse();
                    rsp.CategoryID = Convert.ToInt32(ds.Tables[0].Rows[i]["CarCatID"]);
                    rsp.CategoryName = Convert.ToString(ds.Tables[0].Rows[i]["CarCatName"]);
                    rsp.ModelID = Convert.ToInt32(ds.Tables[0].Rows[i]["CarModelID"]);
                    rsp.ModelName = Convert.ToString(ds.Tables[0].Rows[i]["CarModel"]);
                    response.Add(rsp);
                }
            }
        }
        catch (Exception ex)
        {
            HelperFunctions.LogErrorToLogFile(ex, "[GetModel_Client]:CityID=" + request.CityID + ",ClientCoID=" + request.ClientCoID);
        }

        return response;
    }


    public OL_RentalType GetPackageType(int LookupID)
    {
        OL_RentalType result = new OL_RentalType();

        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"select LookupID, Description, RentalType, AirportDirection, ServiceType, Active from CorIntRentalTypeLookup 
            where active=1 and LookupID = @LookupID ";
            command.Parameters.AddWithValue("LookupID", LookupID);

            InstaCon.Open();
            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader != null)
            {
                DataTable resultTable = new DataTable();
                resultTable.Load(dataReader);
                if (resultTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < resultTable.Rows.Count; rowIndex++)
                    {
                        result.LookupId = Convert.ToInt32(resultTable.Rows[rowIndex]["LookupID"]);
                        result.RentalType = Convert.ToInt32(resultTable.Rows[rowIndex]["RentalType"]);
                        result.ServiceType = Convert.ToString(resultTable.Rows[rowIndex]["ServiceType"]); //service
                        if (result.RentalType == 4)
                        {
                            result.OutstationYN = true;
                        }
                        else
                        {
                            result.OutstationYN = false;
                        }
                        result.AirportDirection = Convert.ToString(resultTable.Rows[rowIndex]["AirportDirection"]);
                        if (result.AirportDirection == "P")
                        {
                            result.Pickup = true;
                        }
                        result.CustomPkgYN = false; //Convert.ToInt32(resultTable.Rows[rowIndex]["CarCompName"]);

                        result.Description = Convert.ToString(resultTable.Rows[rowIndex]["CarCompName"]);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

        return result;
    }

    public OL_PackageResponse GetPackage(OL_PackageRequest request)
    {
        OL_PackageResponse response = new OL_PackageResponse();
        DataSet ds = new DataSet();

        OL_RentalType rentaltyp = new OL_RentalType();
        rentaltyp = GetPackageType(request.LookupID);
        try
        {
            SqlParameter[] param = new SqlParameter[12];
            SqlHelper sql = new SqlHelper();

            int index = -1;
            param[index += 1] = new SqlParameter("@ClientType", "C");
            param[index += 1] = new SqlParameter("@ClientID", request.ClientCoId);
            param[index += 1] = new SqlParameter("@CarCatID", request.CarCatId);
            param[index += 1] = new SqlParameter("@PkgHrs", request.PkgHrs);
            param[index += 1] = new SqlParameter("@CityID", request.CityID);
            param[index += 1] = new SqlParameter("@DateOut", request.PickupDate);
            param[index += 1] = new SqlParameter("@PkgId", request.PkgID);
            param[index += 1] = new SqlParameter("@DateIn", request.PickupDate);
            param[index += 1] = new SqlParameter("@intPkgKMsTrue", 0);
            param[index += 1] = new SqlParameter("@Service", rentaltyp.ServiceType);
            param[index += 1] = new SqlParameter("@OutstationYN", rentaltyp.OutstationYN);
            param[index += 1] = new SqlParameter("@CustomYN", rentaltyp.CustomPkgYN);

            ds = sql.ExecuteDataSet("Prc_GetLocalPkg_Booking", param);

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    response.PkgId = Convert.ToInt32(ds.Tables[0].Rows[i]["PkgId"]);
                    response.PkgRate = Convert.ToDouble(ds.Tables[0].Rows[i]["PkgRate"]);
                    response.ExtraHrRate = Convert.ToDouble(ds.Tables[0].Rows[i]["ExtraHrRate"]);
                    response.ExtraKMRate = Convert.ToDouble(ds.Tables[0].Rows[i]["ExtraKMRate"]);
                    response.OutStationAllowance = Convert.ToDouble(ds.Tables[0].Rows[i]["OutStationAllowance"]);
                    response.NightStayAllowance = Convert.ToDouble(ds.Tables[0].Rows[i]["NightStayAllowance"]);
                    response.ThresholdExtraHr = Convert.ToDouble(ds.Tables[0].Rows[i]["ThresholdExtraHr"]);
                    response.ThresholdExtraKM = Convert.ToDouble(ds.Tables[0].Rows[i]["ThresholdExtraKM"]);
                    response.WaitingCharges = Convert.ToDouble(ds.Tables[0].Rows[i]["WaitingCharges"]);
                    response.ExNgtAmt = Convert.ToDouble(ds.Tables[0].Rows[i]["ExNgtAmt"]);
                    response.ExdayAmt = Convert.ToDouble(ds.Tables[0].Rows[i]["ExdayAmt"]);
                    response.FxdTaxes = Convert.ToDouble(ds.Tables[0].Rows[i]["FxdTaxes"]);
                    response.PkgHrs = Convert.ToInt32(ds.Tables[0].Rows[i]["PkgHrs"]);
                    response.PkgKms = Convert.ToInt32(ds.Tables[0].Rows[i]["PkgKms"]);
                    response.PkgHrs = Convert.ToInt32(ds.Tables[0].Rows[i]["PkgHrs"]);
                    response.NoOfNgts = Convert.ToInt32(ds.Tables[0].Rows[i]["NoOfNgts"]);
                    response.NoOfDys = Convert.ToInt32(ds.Tables[0].Rows[i]["NoOfDys"]);
                }
            }
        }
        catch (Exception ex)
        {
            HelperFunctions.LogErrorToLogFile(ex, "[GetPackage]:ClientCoId=" + request.ClientCoId + ",CarCatId=" + request.CarCatId + ",PkgHrs=" + request.PkgHrs
                + ",CityID=" + request.CityID + ",PickupDate=" + request.PickupDate + ",PkgID=" + request.PkgID + ",DropoffDate=" + request.DropoffDate
                + ",Service=" + rentaltyp.ServiceType + ",OutstationYN=" + rentaltyp.OutstationYN + ",CustomPkgYN=" + rentaltyp.CustomPkgYN);
        }

        return response;
    }

    //public DataSet getCityWiseLocation_Insta(int CityID)
    //{
    //    DataSet ds = new DataSet();
    //    try
    //    {
    //        SqlHelper sql = new SqlHelper();
    //        SqlParameter[] sqlParams = new SqlParameter[1];
    //        sqlParams[0] = new SqlParameter("@cityId", CityID);
    //        ds = sql.ExecuteDataSet("Prc_GetAllCurrentdateLogin", sqlParams);
    //    }
    //    catch (Exception ex)
    //    {
    //        ds = null;
    //    }
    //    return ds;
    //}

    public OL_BookingResponse CreateBooking(OL_BookingRequest booking, OL_PkgCalc calc)
    {
        OL_BookingResponse ol = new OL_BookingResponse();
        int objBookingid = 0;
        //bool outstationYN = false;
        bool SendLinkToGuest = false;
        if (booking.PaymentTerms == "CC")
        {
            booking.Status = "X";
        }

        //if (booking.LookupId == 11)
        //{
        //    booking.Service = "T";
        //}
        //else
        //{
        //    if (booking.RentalType == "4")
        //    {
        //        outstationYN = true;
        //    }
        //}

        //bool customPkgYN = false;
        //if (booking.CustomPkgYN == "yes")
        //{
        //    customPkgYN = true;
        //}


        OL_RentalType rentaltyp = new OL_RentalType();
        rentaltyp = GetPackageType(booking.LookupId);


        int pickup = 0;// objCurrentBookingRentalType.AirportDirection == "P" ? 1 : 0;

        SqlParameter[] param = new SqlParameter[75];
        int index = -1;
        //param[index += 1] = new SqlParameter("@BookIndivID", "C" + booking.ClientCoIndivId);

        param[index += 1] = new SqlParameter("@ClientCoIndivID", booking.ClientCoIndivId);
        param[index += 1] = new SqlParameter("@FacilitatorID", null);
        param[index += 1] = new SqlParameter("@Service", rentaltyp.ServiceType); //service
        param[index += 1] = new SqlParameter("@PaymentMode", booking.PaymentTerms);
        param[index += 1] = new SqlParameter("@PickUpCityID", booking.CityID);
        param[index += 1] = new SqlParameter("@ModelID", booking.ModelId);
        param[index += 1] = new SqlParameter("@PickUpDate", booking.PickupDate);
        param[index += 1] = new SqlParameter("@PickUpTime", booking.PickupTime);
        param[index += 1] = new SqlParameter("@DropOffDate", booking.DropoffDate);
        param[index += 1] = new SqlParameter("@DropOffTime", booking.DropoffTime);

        param[index += 1] = new SqlParameter("@PickUpAdd", booking.PickupAdd.Replace('"', ' ').Replace('\'', ' '));
        param[index += 1] = new SqlParameter("@OutstationYN", rentaltyp.OutstationYN);
        param[index += 1] = new SqlParameter("@AirportType", booking.TerminalType); // to check
        param[index += 1] = new SqlParameter("@FlgtNo", ""); //to check
        param[index += 1] = new SqlParameter("@ReportContact", ""); //to check
        param[index += 1] = new SqlParameter("@VisitCitiesIDs", ""); // to check
        param[index += 1] = new SqlParameter("@IndicatedPkgID", booking.PkgID);
        param[index += 1] = new SqlParameter("@IndicatedPkgHrsTrue", booking.IndicatedPkgHrsTrue);
        param[index += 1] = new SqlParameter("@IndicatedPkgHrs", booking.IndicatedPkgHrs);
        param[index += 1] = new SqlParameter("@IndicatedPkgKMs", booking.IndicatedPkgKMs);

        param[index += 1] = new SqlParameter("@IndicatedExtraHr", booking.ExtraHrRate); //to check
        param[index += 1] = new SqlParameter("@IndicatedExtraKM", booking.ExtraKMRate); //to check
        param[index += 1] = new SqlParameter("@IndicatedNightStayAmt", calc.NightStayAllowance);
        param[index += 1] = new SqlParameter("@IndicatedOutStnAmt", calc.OutstationAmount);
        param[index += 1] = new SqlParameter("@IndicatedPrice", calc.IndicatedPrice);
        param[index += 1] = new SqlParameter("@IndicatedDiscPC", "0"); //to check
        param[index += 1] = new SqlParameter("@IndicatedDepositAmt", calc.IndicatedPrice);
        param[index += 1] = new SqlParameter("@NoNight", booking.NoOfNgts);
        param[index += 1] = new SqlParameter("@ApprovalNo", "");
        param[index += 1] = new SqlParameter("@Status", booking.Status);

        param[index += 1] = new SqlParameter("@Remarks", booking.Remarks.Replace('"', ' ').Replace('\'', ' '));
        param[index += 1] = new SqlParameter("@CreatedBy", booking.UserID);
        param[index += 1] = new SqlParameter("@ContDSID", ""); //to check
        param[index += 1] = new SqlParameter("@HouseNo", ""); //to check
        param[index += 1] = new SqlParameter("@streetNo", ""); //to check
        param[index += 1] = new SqlParameter("@Landmark", "");
        param[index += 1] = new SqlParameter("@PickUpLocation", ""); //to check
        param[index += 1] = new SqlParameter("@SubcategoryId", 0); //to check
        param[index += 1] = new SqlParameter("@trackid", "");
        param[index += 1] = new SqlParameter("@transactionid", booking.TransactionId);

        param[index += 1] = new SqlParameter("@authorizationid", booking.AuthorizationId);
        param[index += 1] = new SqlParameter("@ApprovalAmt", booking.IndicatedPrice);
        param[index += 1] = new SqlParameter("@CCType", 0);//Akhilesh
        param[index += 1] = new SqlParameter("@PaymentStatus", 0);
        param[index += 1] = new SqlParameter("@CCNo", "");
        param[index += 1] = new SqlParameter("@EXPYYMM", "");
        param[index += 1] = new SqlParameter("@bitPreAuthChk", booking.PreAuthNotRequire);
        param[index += 1] = new SqlParameter("@ToNFroYN", 0);
        param[index += 1] = new SqlParameter("@SubID", booking.SubsidiaryId);
        param[index += 1] = new SqlParameter("@bookername", "");

        param[index += 1] = new SqlParameter("@bookerphone", "");
        param[index += 1] = new SqlParameter("@PickUp", pickup);
        param[index += 1] = new SqlParameter("@CustomPkgYN", rentaltyp.CustomPkgYN);
        param[index += 1] = new SqlParameter("@VoucherYN", false); //to check
        param[index += 1] = new SqlParameter("@SecurityCardYN", false); //to check
        param[index += 1] = new SqlParameter("@PromotionCode", ""); //to check
        param[index += 1] = new SqlParameter("@DiscountCode", ""); //to check
        param[index += 1] = new SqlParameter("@PLat", booking.Plat);
        param[index += 1] = new SqlParameter("@PLon", booking.Plon);
        param[index += 1] = new SqlParameter("@LookupID", booking.LookupId);

        param[index += 1] = new SqlParameter("@DLat", 0);
        param[index += 1] = new SqlParameter("@DLon", 0);
        param[index += 1] = new SqlParameter("@GuestPickUpLocation", booking.PickupGeolocation);
        param[index += 1] = new SqlParameter("@OriginCode", "");
        param[index += 1] = new SqlParameter("@ServiceTypeId", booking.ServiceTypeId == 0 ? null : booking.ServiceTypeId);
        param[index += 1] = new SqlParameter("@isPaymateCorporateModuleIndiv", booking.isPaymateCorporateModuleIndiv);
        param[index += 1] = new SqlParameter("@IsNotEligibleForAutoAllocation", false); //booking.IsAllowforAutoAllocation); //false for now will change later
        param[index += 1] = new SqlParameter("@SendLinkToGuest", SendLinkToGuest);
        param[index += 1] = new SqlParameter("@CGSTTaxPercent", calc.CGSTPercent);
        param[index += 1] = new SqlParameter("@SGSTTaxPercent", calc.SGSTPercent);

        param[index += 1] = new SqlParameter("@IGSTTaxPercent", calc.IGSTPercent);
        param[index += 1] = new SqlParameter("@CGSTTaxAMT", calc.CGSTaxAmount);
        param[index += 1] = new SqlParameter("@SGSTTaxAMT", calc.SGSTaxAmount);
        param[index += 1] = new SqlParameter("@IGSTTaxAMT", calc.IGSTaxAmount);
        param[index += 1] = new SqlParameter("@ClientGSTId", calc.ClientGSTId);

        //param[index += 1] = new SqlParameter("@DropOffCityId", 0);
        //param[index += 1] = new SqlParameter("@GSTSurchargeAmount", calc.GSTSurchargeAmount);
        //param[index += 1] = new SqlParameter("@BookingDetailsIncompleteYN", booking.BookingDetailsIncompleteYN);
        //param[index += 1] = new SqlParameter("@IsMinuteWiseBilling", pkg.IsMinuteWiseBilling);

        object status = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CreateBooking", param);

        ol.BookingID = (int)status;

        if (ol.BookingID > 0)
        {
            ol.Status = "Success";
        }
        else
        {
            ol.Status = "Failure";
            ol.ResponseString = "Unable to Create Booking, please try again Later.";
        }

        return ol;
    }


    public OL_IndicatedPrice TaxCalculation(OL_PackageRequest pkg)
    {
        OL_IndicatedPrice objIndicatedPrice = new OL_IndicatedPrice();
        OL_TaxHeads objTaxHeads = new OL_TaxHeads();
        string subSidID = "0";

        string dt = Convert.ToDateTime(pkg.PickupDate).ToShortDateString();

        string responseJSON = GeneralUtility.doGet(GeneralUtility.GetConfigurationValue("TaxSendSMS") + "?ClientCoID=" 
            + pkg.ClientCoId + "&PickupCityID=" + pkg.CityID + "&DateIn=" + dt + "&CarID=0&SubsidiaryID=" + subSidID);
        //  OL_TaxHeads objTaxHeads;
        if (string.IsNullOrEmpty(responseJSON))
        {
            objTaxHeads = null;
        }
        else
        {
            objTaxHeads = JsonConvert.DeserializeObject<OL_TaxHeads>(responseJSON);
            if (objTaxHeads.GSTEnabledYN == "0")
            {
                objTaxHeads.GSTEnabledYN = "false";
            }
            if (objTaxHeads.GSTEnabledYN == "1")
            {
                objTaxHeads.GSTEnabledYN = "true";
            }
        }
        //if (objTaxHeads!=null &&  Convert.ToBoolean(Convert.ToInt16(objTaxHeads.GSTEnabledYN))==true)
        if (objTaxHeads != null && Convert.ToBoolean(objTaxHeads.GSTEnabledYN) == true)
        {
            objIndicatedPrice.CGSTPercent = objTaxHeads.CGSTPercent;
            objIndicatedPrice.SGSTPercent = objTaxHeads.SGSTPercent;
            objIndicatedPrice.IGSTPercent = objTaxHeads.IGSTPercent;
            objIndicatedPrice.CGSTaxAmount = (objIndicatedPrice.IndicatedPrice * objTaxHeads.CGSTPercent) / 100;
            objIndicatedPrice.SGSTaxAmount = (objIndicatedPrice.IndicatedPrice * objTaxHeads.SGSTPercent) / 100;
            objIndicatedPrice.IGSTaxAmount = (objIndicatedPrice.IndicatedPrice * objTaxHeads.IGSTPercent) / 100;
            objIndicatedPrice.IndicatedPrice = objIndicatedPrice.IndicatedPrice + objIndicatedPrice.CGSTaxAmount + objIndicatedPrice.SGSTaxAmount 
                + objIndicatedPrice.IGSTaxAmount;
            objIndicatedPrice.ClientGSTId = objTaxHeads.ClientGSTId == "" ? 0 : Convert.ToInt32(objTaxHeads.ClientGSTId);
        }
        else
        {
            objIndicatedPrice.CGSTPercent = 0;
            objIndicatedPrice.SGSTPercent = 0;
            objIndicatedPrice.IGSTPercent = 0;
            objIndicatedPrice.CGSTaxAmount = 0;
            objIndicatedPrice.SGSTaxAmount = 0;
            objIndicatedPrice.IGSTaxAmount = 0;
            objIndicatedPrice.ClientGSTId = 0;

        }
        return objIndicatedPrice;
    }

    private OL_IndicatedPrice getIndicatedPrice(OL_PackageRequest request, OL_PackageResponse response)
    {
        OL_IndicatedPrice objIndicatedPrice = new OL_IndicatedPrice();

        OL_RentalType rentaltyp = new OL_RentalType();
        rentaltyp = GetPackageType(request.LookupID);

        double indicatedPrice = 0.0;
        if (rentaltyp.ServiceType == "C" && rentaltyp.OutstationYN)
        {
            // outstation calc
            objIndicatedPrice.IndicatedNightStayAllowance = (response.NoOfNgts * response.NightStayAllowance);
            objIndicatedPrice.IndicatedOutstationAmount = (response.PkgHrs * response.OutStationAllowance);
            indicatedPrice = (response.PkgHrs * response.PkgRate) + objIndicatedPrice.IndicatedNightStayAllowance +
                objIndicatedPrice.IndicatedOutstationAmount;
        }
        else if (rentaltyp.ServiceType == "C" && !rentaltyp.OutstationYN && !rentaltyp.CustomPkgYN)
        {
            // local
            double extraHr = response.PkgHrs > response.PkgHrs ? response.PkgHrs - response.PkgHrs : 0;
            objIndicatedPrice.IndicatedNightStayAllowance = (response.NoOfNgts * response.NightStayAllowance);
            indicatedPrice = (response.PkgRate) + (response.ExtraHrRate * extraHr) +
               objIndicatedPrice.IndicatedNightStayAllowance;

        }
        else if (rentaltyp.ServiceType == "A")
        {
            objIndicatedPrice.IndicatedNightStayAllowance = (response.NoOfNgts * response.NightStayAllowance);
            indicatedPrice = (response.PkgRate) +
                objIndicatedPrice.IndicatedNightStayAllowance;
        }

        objIndicatedPrice.IndicatedPrice = indicatedPrice;

        OL_IndicatedPrice pkg = new OL_IndicatedPrice();
        pkg = TaxCalculation(request);

        return objIndicatedPrice;
    }

    public string formatTime(string timeformat)
    {
        string tim = "", returntim = "";

        if (timeformat.Length == 1)
        {
            tim = "000" + timeformat;
        }
        else if (timeformat.Length == 2)
        {
            tim = "00" + timeformat;
        }
        else if (timeformat.Length == 3)
        {
            tim = "00" + timeformat;
        }
        else
        {
            tim = timeformat;
        }

        returntim = tim.Substring(0, 2) + ":" + tim.Substring(2, 4);


        return returntim;
    }

    public OL_PackageInterval GetPkgInterval(OL_BookingRequest booking)
    {
        OL_RentalType rentaltyp = new OL_RentalType();
        rentaltyp = GetPackageType(booking.LookupId);

        OL_PackageInterval objPkgInterval = new OL_PackageInterval();

        int ClientCOID = GetClientCoID(booking.ClientCoIndivId, true);
        SqlParameter[] param = new SqlParameter[16];
        param[0] = new SqlParameter("@ClientCoID", ClientCOID);
        param[1] = new SqlParameter("@Service", rentaltyp.ServiceType);
        param[2] = new SqlParameter("@BillingBasis", "pp");
        param[3] = new SqlParameter("@OutstationYN", rentaltyp.OutstationYN);
        param[4] = new SqlParameter("@CustomYN", rentaltyp.CustomPkgYN);
        param[5] = new SqlParameter("@GuestDateOut", booking.PickupDate.ToString("yyyy-MM-dd"));
        param[6] = new SqlParameter("@GuestDateIn", booking.DropoffDate.ToString("yyyy-MM-dd"));
        param[7] = new SqlParameter("@GuestTimeOut", formatTime(booking.PickupTime)); //.ToString("HH:mm"));
        param[8] = new SqlParameter("@GuestTimeIn", formatTime(booking.DropoffTime)); //.ToString("HH:mm"));
        param[9] = new SqlParameter("@GarageDateOut", booking.PickupDate.ToString("yyyy-MM-dd"));
        param[10] = new SqlParameter("@GarageDateIn", booking.DropoffDate.ToString("yyyy-MM-dd"));
        param[11] = new SqlParameter("@GarageTimeOut", formatTime(booking.PickupTime)); //.ToString("HH:mm"));
        param[12] = new SqlParameter("@GarageTimeIn", formatTime(booking.DropoffTime)); //.ToString("HH:mm"));

        param[13] = new SqlParameter("@PkgHrTrue", SqlDbType.Real); // Can we try this?
        param[13].Direction = ParameterDirection.Output;

        param[14] = new SqlParameter("@intNoNights", SqlDbType.Int);
        param[14].Direction = ParameterDirection.Output;

        param[15] = new SqlParameter("@intNoDays", SqlDbType.Int);
        param[15].Direction = ParameterDirection.Output;

        //Car Category
        int status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prcGetTimeInterval", param);


        objPkgInterval.PkgHrs = Convert.ToDouble(param[13].Value);
        objPkgInterval.NoOfNights = Convert.ToInt16(param[14].Value);
        objPkgInterval.NoOfDays = Convert.ToInt16(param[15].Value);

        return objPkgInterval;
    }

    public List<OL_RentalType> GetRentalType()
    {
        DataSet ds = new DataSet();
        List<OL_RentalType> objListOfRentalTypeOL = new List<OL_RentalType>();

        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            command.CommandText = @"select b.LookupID,b.Description,b.RentalType,b.ServiceType, B.AirportDirection 
            from CorIntRentalTypeMaster as A inner join CorIntRentalTypeLookup as B on A.RentalType=b.RentalType where a.Active =1 and b.Active =1 ";
            //command.Parameters.AddWithValue("ProviderID", model.ProviderID);
            InstaCon.Open();

            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader != null)
            {
                DataTable resultTable = new DataTable();
                resultTable.Load(dataReader);
                if (resultTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < resultTable.Rows.Count; rowIndex++)
                    {
                        OL_RentalType rentalType = new OL_RentalType();
                        rentalType.LookupId = Convert.ToInt16(resultTable.Rows[rowIndex]["LookupID"]);
                        rentalType.Description = resultTable.Rows[rowIndex]["Description"].ToString();
                        rentalType.RentalType = Convert.ToInt16(resultTable.Rows[rowIndex]["RentalType"]);
                        rentalType.ServiceType = resultTable.Rows[rowIndex]["ServiceType"].ToString(); //service
                        rentalType.AirportDirection = resultTable.Rows[rowIndex]["AirportDirection"].ToString();


                        //if (rentalType.ServiceType == "A")
                        //{
                        //    rentalType.PackageType = PackageType.Airport;
                        //}
                        //else if (rentalType.RentalType == Convert.ToInt16(RentalType.OutStation) && rentalType.ServiceType == "C")
                        //{
                        //    rentalType.PackageType = PackageType.Outsatation;
                        //}
                        //else if (rentalType.RentalType == Convert.ToInt16(RentalType.OneWayCityTransfer) && rentalType.ServiceType == "T")
                        //{
                        //    rentalType.PackageType = PackageType.CityTranster;
                        //}
                        //else
                        //{
                        //    rentalType.PackageType = PackageType.Local;
                        //}
                        objListOfRentalTypeOL.Add(rentalType);
                    }
                }
            }
        }
        catch (Exception e)
        {

        }
        return objListOfRentalTypeOL;
    }

    public int GetClientCoID(int ID, bool GuestYN) //if GuestYN = true then Clientcoindivid else bookingid
    {
        int ClientCoID = 0;
        try
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlConnection InstaCon = new SqlConnection(connectionString);

            SqlCommand command = InstaCon.CreateCommand();
            if (GuestYN)
            {
                command.CommandText = @"select ClientCoID from CORIntClientCoIndivMaster where ClientCoIndivID = @ID ";
            }
            else
            {
                command.CommandText = @"select ClientCoID from corintCarBooking as CB 
                inner join CORIntClientCoIndivMaster as Indiv on CB.clientcoindivid = Indiv.clientcoindivid
                where CB.bookingid = @ID ";
            }

            command.Parameters.AddWithValue("ID", ID);

            InstaCon.Open();
            SqlDataReader dataReader = command.ExecuteReader();
            if (dataReader != null)
            {
                DataTable resultTable = new DataTable();
                resultTable.Load(dataReader);
                if (resultTable.Rows.Count > 0)
                {
                    for (int rowIndex = 0; rowIndex < resultTable.Rows.Count; rowIndex++)
                    {
                        ClientCoID = Convert.ToInt32(resultTable.Rows[rowIndex]["ClientCoID"]);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

        return ClientCoID;
    }
}
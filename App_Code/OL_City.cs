﻿/// <summary>
/// Summary description for OL_City
/// </summary>
public class OL_City
{
    public int CityId { get; set; }
    public string CityName { get; set; }
    public int StateID { get; set; }
    public double CityLat { get; set; }
    public double CityLon { get; set; }
    public int ProviderID { get; set; }
    public bool CorDriveYN { get; set; }
    public string Region { get; set; }
    public string FName { get; set; }
    public string MName { get; set; }
    public string LName { get; set; }
    public int DesigID { get; set; }
    public string Address { get; set; }
    public string Phone { get; set; }
    public string EmailID { get; set; }
    public string ServiceTaxNo { get; set; }
    public string TanNo { get; set; }
    public string GSTIN { get; set; }
    public string GSTINAddress { get; set; }
    public int UserID { get; set; }
}

public class OL_CityID
{
    public int CityId { get; set; }
    public string CityName { get; set; }
    public int ProviderID { get; set; }
}

public class OL_StateID
{
    public int StateID { get; set; }
    public string StateName { get; set; }
}
﻿using System;
/// <summary>
/// Summary description for OL_Booking
/// </summary>
public class OL_BookingRequest
{
    public int ClientCoIndivId { get; set; }
    public int UserID { get; set; }
    public string BookingId { get; set; }
    //public string Service { get; set; }
    public DateTime PickupDate { get; set; }
    public string PickupTime { get; set; }
    public DateTime DropoffDate { get; set; }
    public string DropoffTime { get; set; }
    public string CityID { get; set; }
    //public int UnitId { get; set; }
    public string ModelId { get; set; }
    public string PickupAdd { get; set; }

    public string Plat { get; set; }
    public string Plon { get; set; }
    public string Dlat { get; set; }
    public string Dlon { get; set; }
    public string PickupGeolocation { get; set; }
    public string DropoffGeolocation { get; set; }
    public string Remarks { get; set; }

    //public string CustomPkgYN { get; set; }
    //public string OutstationYN { get; set; }
    public string RentalTypeDescription { get; set; }
    public string AirportDirection { get; set; }
    public int LookupId { get; set; }
    public string TerminalType { get; set; }
    public string RentalLookupDescription { get; set; }

    public string SubsidiaryId { get; set; }
    public string TransactionId { get; set; }
    public string AuthorizationId { get; set; }
    public string RentalType { get; set; }
    public string SendSMS { get; set; }
    public string SendEmail { get; set; }
    public string Status { get; set; }

    public string PkgID { get; set; }
    public string IndicatedPkgHrs { get; set; }
    public string IndicatedPkgKMs { get; set; }
    public string IndicatedPkgHrsTrue { get; set; }
    public string ExtraHrRate { get; set; }
    public string ExtraKMRate { get; set; }
    public string NoOfNgts { get; set; }
    public string IndicatedPrice { get; set; }
    public string PreAuthNotRequire { get; set; }
    public string PaymentTerms { get; set; }
    public string PreauthAmount { get; set; }
    public int? ServiceTypeId { get; set; }
    public bool isPaymateCorporateModuleIndiv { get; set; }
    public string IsAllowforAutoAllocation { get; set; }
    //public Boolean IsVip { get; set; }
    //public int DropOffCityId { get; set; }

    //GST Details ******
    public double IndicatedCGSTTaxPercent { get; set; }
    public double IndicatedSGSTTaxPercent { get; set; }
    public double IndicatedIGSTTaxPercent { get; set; }
    public double IndicatedCGSTTaxAmt { get; set; }
    public double IndicatedSGSTTaxAmt { get; set; }
    public double IndicatedIGSTTaxAmt { get; set; }
    public int IndicatedClientGSTId { get; set; }

    //public int PkgID { get; set; }
    //public int PkgHrs { get; set; }
    //public int PkgKms { get; set; }
    //public int ExtraHrRate { get; set; }
    //public int ExtraKMRate { get; set; }
    //public int NoOfNgts { get; set; }
    //public bool IsMinuteWiseBilling { get; set; }
}


public class OL_PkgCalc
{
    public double NightStayAllowance { get; set; }
    public double OutstationAmount { get; set; }
    public double IndicatedPrice { get; set; }
    public double CGSTPercent { get; set; }
    public double SGSTPercent { get; set; }
    public double IGSTPercent { get; set; }
    public double CGSTaxAmount { get; set; }
    public double SGSTaxAmount { get; set; }
    public double IGSTaxAmount { get; set; }
    public int ClientGSTId { get; set; }

}

public class OL_BookingResponse
{
    public string Status { get; set; }
    public string ResponseString { get; set; }
    public int BookingID { get; set; }
}

public class OL_ModelClientRequest
{
    public int ClientCoID { get; set; }
    public int CityID { get; set; }
}

public class OL_ModelClientReponse
{
    public int ModelID { get; set; }
    public string ModelName { get; set; }
    public int CategoryID { get; set; }
    public string CategoryName { get; set; }
}


public class OL_RentalType
{
    public int LookupId { get; set; }
    public int RentalType { get; set; }
    public string Description { get; set; }
    public string ServiceType { get; set; }
    //public string PackageType { get; set; }
    public string AirportDirection { get; set; }
    public bool OutstationYN { get; set; }
    public bool Pickup { get; set; }
    public bool CustomPkgYN { get; set; }

}

public class PackageType
{
    public const string Local = "C";
    public const string Airport = "A";
    public const string Outsatation = "O";
    public const string CityTranster = "T";
    public const string MinuteWise = "M";
}

public enum RentalType
{
    Airport = 0,
    OutStation = 4,
    OneWayCityTransfer = 5,
    Local
}
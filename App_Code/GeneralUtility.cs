﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

/// <summary>
/// Summary description for GeneralUtility
/// </summary>
public class GeneralUtility
{
    public GeneralUtility()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static string GetConfigurationValue(string key)
    {
        return ConfigurationManager.AppSettings[key];
    }

    public static string doGet(string fullUrl, Dictionary<string, string> headers = null)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullUrl);
        if (headers != null)
            foreach (string key in headers.Keys)
                request.Headers.Add(key, headers[key]);
        request.Method = "GET";
        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                string responseFromServer = reader.ReadToEnd();
                return responseFromServer;
            }
        }
    }
}
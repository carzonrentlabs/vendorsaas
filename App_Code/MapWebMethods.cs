﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;


/// <summary>
/// Summary description for MapWebMethods
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class MapWebMethods : System.Web.Services.WebService
{
    public MapWebMethods()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    private string GetJSONRequest()
    {
        return new System.IO.StreamReader(Context.Request.InputStream).ReadToEnd();
    }

    private String sendJSONResponse(String json)
    {
        Context.Response.ContentType = "application/json";
        Context.Response.Output.Write(json);
        Context.Response.Flush();
        Context.Response.SuppressContent = true;
        Context.ApplicationInstance.CompleteRequest();
        // Context.Response.End();
        return string.Empty;
    }

    public class GenericResponse
    {
        public string status;
        public string failureMessage;
        public string OTP;
    }

    [WebMethod(Description = "Save Category")]
    [ScriptMethod]
    public string SaveCategory()
    {
        string requestStr = GetJSONRequest();
        OL_Category request = new JavaScriptSerializer().Deserialize<OL_Category>(requestStr);
        EventData dll = new EventData();

        GenericResponse response = new GenericResponse();

        response = dll.SaveCategory(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod(Description = "Update Category")]
    [ScriptMethod]
    public string UpdateCategory()
    {
        string requestStr = GetJSONRequest();
        OL_Category request = new JavaScriptSerializer().Deserialize<OL_Category>(requestStr);
        EventData dll = new EventData();

        GenericResponse response = new GenericResponse();

        response = dll.UpdateCategory(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }


    [WebMethod(Description = "Get Cartegory List")]
    [ScriptMethod]
    public string GetCategoryList()
    {
        string requestStr = GetJSONRequest();
        OL_Category request = new JavaScriptSerializer().Deserialize<OL_Category>(requestStr);
        EventData dll = new EventData();

        List<OL_Category> response = new List<OL_Category>();

        response = dll.GetCategoryDetails(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }


    [WebMethod(Description = "Save Model")]
    [ScriptMethod]
    public string SaveModel()
    {
        string requestStr = GetJSONRequest();
        OL_Model request = new JavaScriptSerializer().Deserialize<OL_Model>(requestStr);
        EventData dll = new EventData();
        GenericResponse response = new GenericResponse();

        response = dll.SaveModel(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod(Description = "Update Model")]
    [ScriptMethod]
    public string UpdateModel()
    {
        string requestStr = GetJSONRequest();
        OL_Model request = new JavaScriptSerializer().Deserialize<OL_Model>(requestStr);
        EventData dll = new EventData();

        GenericResponse response = new GenericResponse();

        response = dll.UpdateModel(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }


    [WebMethod(Description = "Get Model Details")]
    [ScriptMethod]
    public string GetModelList()
    {
        string requestStr = GetJSONRequest();
        OL_Model request = new JavaScriptSerializer().Deserialize<OL_Model>(requestStr);
        EventData dll = new EventData();

        List<OL_Model> response = new List<OL_Model>();

        response = dll.GetModelDetails(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod(Description = "Get Model for Drop down List")]
    [ScriptMethod]
    public string GetModel()
    {
        string requestStr = GetJSONRequest();
        OL_ModelID request = new JavaScriptSerializer().Deserialize<OL_ModelID>(requestStr);
        EventData dll = new EventData();

        List<OL_ModelID> response = new List<OL_ModelID>();

        response = dll.GetModelList(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod(Description = "Get Package Details")]
    [ScriptMethod]
    public string GetPackage()
    {
        string requestStr = GetJSONRequest();
        OL_PackageRequest request = new JavaScriptSerializer().Deserialize<OL_PackageRequest>(requestStr);
        EventData dll = new EventData();

        OL_PackageResponse response = new OL_PackageResponse();

        response = dll.GetPackage(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod(Description = "Calculate Booking Amount")]
    [ScriptMethod]
    public string EstimateBookingAmt()
    {
        string requestStr = GetJSONRequest();
        OL_PackageRequest request = new JavaScriptSerializer().Deserialize<OL_PackageRequest>(requestStr);
        EventData dll = new EventData();

        OL_PackageResponse response = new OL_PackageResponse();

        response = dll.GetPackage(request);

        OL_IndicatedPrice ol = new OL_IndicatedPrice();
        ol = dll.TaxCalculation(request); //get tax and calculate indicated price

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }


    [WebMethod]
    [ScriptMethod]
    public string SaveCity()
    {
        string requestStr = GetJSONRequest();
        OL_City request = new JavaScriptSerializer().Deserialize<OL_City>(requestStr);
        EventData dll = new EventData();
        GenericResponse response = new GenericResponse();

        response = dll.SaveCity(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod]
    [ScriptMethod]
    public string UpdateCity()
    {
        string requestStr = GetJSONRequest();
        OL_City request = new JavaScriptSerializer().Deserialize<OL_City>(requestStr);
        EventData dll = new EventData();

        GenericResponse response = new GenericResponse();

        response = dll.UpdateCity(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod]
    [ScriptMethod]
    public string GetCities()
    {
        string requestStr = GetJSONRequest();
        OL_CityID request = new JavaScriptSerializer().Deserialize<OL_CityID>(requestStr);
        EventData dll = new EventData();

        List<OL_CityID> response = new List<OL_CityID>();

        response = dll.GetCityList(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod]
    [ScriptMethod]
    public string CreateBooking()
    {
        string requestStr = GetJSONRequest();
        OL_BookingRequest request = new JavaScriptSerializer().Deserialize<OL_BookingRequest>(requestStr);
        EventData dll = new EventData();

        OL_BookingResponse response = new OL_BookingResponse();
        //OL_Pkg pkg = new OL_Pkg();
        OL_PkgCalc calc = new OL_PkgCalc();

        response = dll.CreateBooking(request, calc);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod]
    [ScriptMethod]
    public string GetModel_Client()
    {
        string requestStr = GetJSONRequest();
        OL_ModelClientRequest request = new JavaScriptSerializer().Deserialize<OL_ModelClientRequest>(requestStr);

        HelperFunctions.LogInfoToLogFile("GetModel_Client", "CityID=" + request.CityID + ",ClientCoID=" + request.ClientCoID);

        EventData dll = new EventData();

        List<OL_ModelClientReponse> response = new List<OL_ModelClientReponse>();

        response = dll.GetModel_Client(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }


    [WebMethod]
    [ScriptMethod]
    public string GetRentalType()
    {
        //string requestStr = GetJSONRequest();
        //OL_RentalType request = new JavaScriptSerializer().Deserialize<OL_RentalType>(requestStr);

        HelperFunctions.LogInfoToLogFile("GetRentalType", "");

        EventData dll = new EventData();

        List<OL_RentalType> response = new List<OL_RentalType>();

        response = dll.GetRentalType();

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod]
    [ScriptMethod]
    public string GetState()
    {
        //string requestStr = GetJSONRequest();
        //OL_StateID request = new JavaScriptSerializer().Deserialize<OL_StateID>(requestStr);
        EventData dll = new EventData();

        List<OL_StateID> response = new List<OL_StateID>();

        response = dll.GetStateList();

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

    [WebMethod]
    [ScriptMethod]
    public string GetCarComp()
    {
        string requestStr = GetJSONRequest();
        OL_CarCompID request = new JavaScriptSerializer().Deserialize<OL_CarCompID>(requestStr);
        EventData dll = new EventData();

        List<OL_CarCompID> response = new List<OL_CarCompID>();

        response = dll.GetCarCompList(request);

        string responseStr = new JavaScriptSerializer().Serialize(response);
        return sendJSONResponse(responseStr);
    }

}
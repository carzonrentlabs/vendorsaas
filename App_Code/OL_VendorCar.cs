﻿using System;

/// <summary>
/// Summary description for OL_VendorCar
/// </summary>
public class OL_VendorCar
{
    public int VendorID { get; set; }
    public int VendorCarID { get; set; }
    public int ModelID { get; set; }
    public string CarNO { get; set; }
    public bool ACYN { get; set; }
    public string PaymentOption { get; set; }
    public int FuelTypeID { get; set; }
    public DateTime FitnessDate { get; set; }
    public string Grade { get; set; }
    public DateTime ManufacturingDate { get; set; }
    public DateTime InsuranceExpiryDate { get; set; }
    public decimal HomeLocationLat { get; set; }
    public decimal HomeLocationLon { get; set; }
    public DateTime PermitDate { get; set; }
    public int DefaultChauffID { get; set; }
}
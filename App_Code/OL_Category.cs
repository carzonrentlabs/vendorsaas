﻿using System;

/// <summary>
/// Summary description for OL_Category
/// </summary>
/// 
public class OL_Category
{
    public int CategoryID { get; set; }
    public string CategoryName { get; set; }
    public string CategoryGroup { get; set; }
    public int ProviderID { get; set; }
    public int UserID { get; set; }
    public DateTime CreateDate { get; set; }
    public int CreatedBy { get; set; }
    public DateTime ModifyDate { get; set; }
    public int ModifiedBy { get; set; }
}
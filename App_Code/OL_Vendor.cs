﻿using System;

/// <summary>
/// Summary description for OL_Vendor
/// </summary>
public class OL_Vendor
{
    public int VendorID { get; set; }
    public string VendorName { get; set; }
    public string Address { get; set; }
    public int CityID { get; set; }
    public int ProviderID { get; set; }
    public string Fname { get; set; }
    public string Mname { get; set; }
    public string Lname { get; set; }
    public string Phone { get; set; }
    public string EmailID { get; set; }
    public string PANNO { get; set; }
    public DateTime ContractStartDate { get; set; }
    public DateTime ContractEndDate { get; set; }
    public string IFFCCOde { get; set; }
    public string BranchName { get; set; }
    public string BankAccountNO { get; set; }
    public string BenificeryName { get; set; }
    public string MICRCode { get; set; }
    public string BankAddress { get; set; }

}